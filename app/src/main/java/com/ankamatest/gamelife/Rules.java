package com.ankamatest.gamelife;

import java.io.Serializable;

public class Rules implements Serializable {

    // Game parameters
    private int _RefreshTimer = 1000;
    private int _Cols = 20;
    private int _Rows = 20;
    private float _RndInitialLife = 15f;
    private int _MinNeighbour = 1;
    private int _MaxNeighbour = 5;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    Rules() {

    }

    Rules(int sRefreshTimer, int sCols, int sRows, float sRndInitLife, int sMinNeighbour, int sMaxNeighbour) {
        if (sRefreshTimer >= 1000) _RefreshTimer = sRefreshTimer;
        setCols(sCols);
        setRows(sRows);
        if (sRndInitLife >= 0f && sRndInitLife <= 100f) _RndInitialLife = sRndInitLife;
        if (sMinNeighbour >= 0 && sMinNeighbour <= 8) _MinNeighbour = sMinNeighbour;
        if (sMaxNeighbour >= 0 && sMaxNeighbour <= 8) _MaxNeighbour = sMaxNeighbour;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Getter
    protected int getRefreshTimer() {
        return _RefreshTimer;
    }

    protected int getCols() {
        return _Cols;
    }

    protected int getRows() {
        return _Rows;
    }

    protected int getBoardSize() {
        return _Cols * _Rows;
    }

    protected float getRndInitialLife() {
        return _RndInitialLife;
    }

    protected int getMinNeighbour() {
        return _MinNeighbour;
    }

    protected int getMaxNeighbour() {
        return _MaxNeighbour;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Setter
    protected void setCols(int sCols) {
        if (sCols > 0 && sCols <= 50) {
            _Cols = sCols;
        }
    }

    protected void setRows(int sRows) {
        if (sRows > 0 && sRows <= 50) {
            _Rows = sRows;
        }
    }
}
