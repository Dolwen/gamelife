package com.ankamatest.gamelife;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, Runnable {

    private static final String TAG = "GameLife";
    private static final String FILE_NAME = "gamelife_file";

    // UI
    private Button _BtnRandomized;
    private Button _BtnPause;
    private Button _BtnDyingNewBorn;
    private Button _BtnSave;
    private Button _BtnLoad;
    private Button _BtnConnect;
    private EditText _EditCols;
    private EditText _EditRows;
    private Switch _SwtAutosave;
    private TextView _TextBoardGame;
    private final Handler _Handler = new Handler();

    // Game parameters
    private Rules _Rule = new Rules();
    private boolean _IsPause = false;
    private boolean _IsDebugMode = true;
    private boolean _IsAutoSave = false;

    // Server parameters
    private String _Adresse = "192.168.0.1";
    private int _Port = 8080;

    // List of active Cells
    private List<Cell> _Cells = new ArrayList<Cell>();

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MAIN
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set TextView to show the board
        _TextBoardGame = findViewById(R.id.textBoardGame);

        // Set Button action
        _BtnRandomized = findViewById(R.id.btRandomized);
        _BtnRandomized.setOnClickListener(this);
        _BtnPause = findViewById(R.id.btPause);
        _BtnPause.setOnClickListener(this);
        _BtnDyingNewBorn = findViewById(R.id.btDyingNewBorn);
        _BtnDyingNewBorn.setOnClickListener(this);
        _BtnLoad = findViewById(R.id.btLoad);
        _BtnLoad.setOnClickListener(this);
        _BtnSave = findViewById(R.id.btSave);
        _BtnSave.setOnClickListener(this);
        _BtnConnect = findViewById(R.id.btConnect);
        _BtnConnect.setOnClickListener(this);

        // Set Switch action
        _SwtAutosave = findViewById(R.id.swAutosave);
        _SwtAutosave.setOnCheckedChangeListener(this);

        // Set EditText callback
        _EditCols = findViewById(R.id.editCol);
        _EditCols.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                _IsPause = true;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tCols = _EditCols.getText().toString();
                if (!tCols.isEmpty()) {
                    _Rule.setCols(Integer.valueOf(tCols));
                    initGame();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
        _EditRows = findViewById(R.id.editRow);
        _EditRows.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                _IsPause = true;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tRows = _EditRows.getText().toString();
                if (!tRows.isEmpty()) {
                    _Rule.setRows(Integer.valueOf(tRows));
                    initGame();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Initialize Game sequence
        initGame();

        // Set a run handler with a refresh delay
        _Handler.postDelayed(this, _Rule.getRefreshTimer());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // OVERRRIDE
    @Override
    public void onClick(View v) {
        if (v == _BtnRandomized) {
            // Start a new game
            _IsPause = true;
            initGame();
        } else if (v == _BtnPause) {
            // Pause / UnPause game
            _IsPause = !_IsPause;
        } else if (v == _BtnDyingNewBorn) {
            // Show / Hide cell life transition
            _IsDebugMode = !_IsDebugMode;
            _BtnDyingNewBorn.setText("Transition: " + _IsDebugMode);
        } else if (v == _BtnSave) {
            // Save current game stat
            saveGame();
        } else if (v == _BtnLoad) {
            // Load last saved game
            loadGame();
        } else if (v == _BtnConnect) {
            // Start a connection with a server to retrieved a game
            Client tClient = new Client(_Adresse, _Port, _Cells, _Rule, this);
            tClient.execute();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView == _SwtAutosave) {
            if (isChecked) {
                _IsAutoSave = true;
            } else {
                _IsAutoSave = false;
            }
        }
    }

    @Override
    public void run() {
        if (!_IsPause) {
            simulateLife();
            showBoard();
            updateLifeStatus();
            if (_IsAutoSave) {
                saveGame();
            }
        }
        _Handler.postDelayed(this, _Rule.getRefreshTimer());
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveGame();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE
    private void initGame() {
        // Generate the board with no life cell
        generateBoard();

        // Generate and modify active cells if needed
        generateLifeCells();

        // Show Game board
        showBoard();
    }

    private void generateBoard() {
        // Clear Cells list
        _Cells.clear();

        // Generate Cells
        for (int i = 0; i < _Rule.getRows() ; i++) {
            for (int j = 0; j < _Rule.getCols() ; j++) {
                Cell tCell = new Cell(j, i);
                _Cells.add(tCell);
            }
        }
    }

    private void generateLifeCells() {
        int tTotalCells = _Rule.getBoardSize();
        int tTotalLifeCells = (int)Math.ceil(tTotalCells * _Rule.getRndInitialLife() / 100);

        for (int i = tTotalLifeCells ; i > 0 ; i--) {
            int tCpt = 0;
            boolean tSuccess = false;
            while (!tSuccess) {
                int tX = (int)(Math.floor(Math.random() * _Rule.getCols()));
                int tY = (int)(Math.floor(Math.random() * _Rule.getRows()));
                tSuccess = addLifeCell(tX, tY);

                // Security : Break While if no cell is found
                tCpt++;
                if (tCpt > 100) {
                    //Log.e(TAG, "GenerateLifeCells() - An Error occurred in iteration!");
                    System.out.println("GenerateLifeCells() - An Error occurred in iteration!");
                    break;
                }
            }
        }
    }

    private boolean addLifeCell(int sX, int sY) {
        for (Cell k : _Cells) {
            Cell tCell = k.findCell(sX, sY);
            if (tCell != null) {
                return tCell.setInitialLife();
            }
        }

        return false;
    }

    private void simulateLife() {
        for (Cell k : _Cells) {
            k.evolution(_Rule, _Cells);
        }
    }

    private void updateLifeStatus() {
       for (Cell k : _Cells) {
           k.updateStat();
       }
    }

    private void showBoard() {
        StringBuilder tRow = new StringBuilder(" \n");
        int tCpt = 0;
        for (Cell k : _Cells) {
            if (tCpt == 0) {
                tRow.append("|");
            }

            tRow.append(k.show(_IsDebugMode));

            tCpt++;
            if (tCpt == _Rule.getCols()) {
                tRow.append("\n");
                tCpt = 0;
            }
        }

        // Show final string in console and screen
        //Log.d(TAG, tRow.toString());
        System.out.println(tRow.toString());
        _TextBoardGame.setText(tRow.toString());
    }

    private void saveGame() {
        try {
            FileOutputStream outputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream objOutputStream = new ObjectOutputStream(outputStream);

            // Save Rules
            objOutputStream.writeObject(_Rule);

            // Save all current Cells
            for (Cell k : _Cells) {
                objOutputStream.writeObject(k);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadGame() {
        // Clear Cells list before loading
        _Cells.clear();

        try {
            FileInputStream inputStream = openFileInput(FILE_NAME);
            ObjectInputStream objInputStream =  new ObjectInputStream(inputStream);

            boolean tNext = true;
            boolean tFirstEntry = true;

            // Load each saved Cell and add each to the List
            while(tNext) {
                if(inputStream.available() != 0) {
                    Object tObj = objInputStream.readObject();
                    if(tObj != null) {
                        if (tFirstEntry) {
                            // Get Rules at First
                            _Rule = (Rules)tObj;
                            tFirstEntry = false;
                        } else {
                            // Then each Cell
                            _Cells.add((Cell)tObj);
                        }
                    } else {
                        tNext = false;
                    }
                } else {
                    tNext = false;
                }
            }
        } catch (IOException io) {
            io.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Refresh Game Board
        showBoard();
    }
}
