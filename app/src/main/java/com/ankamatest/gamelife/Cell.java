package com.ankamatest.gamelife;

import java.io.Serializable;
import java.util.List;

public class Cell implements Serializable {

    enum Stat {
        None,
        Alive,
        IsDying,
        NewBorn
    }

    private int _Col = 0;
    private int _Row = 0;
    private Stat _Stat = Stat.None;
    private Stat _OldStat = Stat.None;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    Cell() {

    }

    Cell(int sCol, int sRow) {
        if (sCol >= 0) _Col = sCol;
        if (sRow >= 0) _Row = sRow;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PROTECTED
    protected void updateStat() {
        _OldStat = _Stat;
    }

    protected void evolution(Rules sRules, List<Cell> sCells) {
        int tTotal = 0;
        switch (_OldStat) {
            case None: {
                    tTotal = countNeighbors(sRules, sCells);
                    if (tTotal >= sRules.getMinNeighbour() && tTotal <= sRules.getMaxNeighbour()) {
                        setStat(Stat.NewBorn);
                    }
                }
                break;
            case Alive: {
                    tTotal = countNeighbors(sRules, sCells);
                    if (tTotal < sRules.getMinNeighbour() || tTotal > sRules.getMaxNeighbour()) {
                        setStat(Stat.IsDying);
                    }
                }
                break;
            case IsDying: {
                    setStat(Stat.None);
                }
                break;
            case NewBorn: {
                    setStat(Stat.Alive);
                }
                break;
        }

        //Log.d("Cell", "[" + _Row + "-" + _Col + "] t=" + tTotal + " old stat=" + _OldStat + " new stat=" + _Stat);
    }

    protected Cell findCell(int sCol, int sRow) {
        if (_Col == sCol && _Row == sRow) {
            return this;
        }

        return null;
    }

    protected String show(boolean sDebugMode) {
        String rReturn = " |";
        switch (_Stat) {
            case Alive:
                rReturn = "o|";
                break;
            case IsDying:
                if (sDebugMode) {
                    rReturn = "x|";
                } else {
                    rReturn = " |";
                }
                break;
            case NewBorn:
                if (sDebugMode) {
                    rReturn = "n|";
                } else {
                    rReturn = "o|";
                }
                break;
        }

        return rReturn;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Setter
    protected void setStat(Stat sStat) {
        _Stat = sStat;
    }

    protected boolean setInitialLife() {
        if (_Stat == Stat.None) {
            setStat(Stat.Alive);
            _OldStat = _Stat;
            return true;
        }

        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Getter
    protected Stat getOldStat() {
        return _OldStat;
    }

    protected int getCol() {
        return _Col;
    }

    protected int getRow() {
        return _Row;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // PRIVATE
    private int countNeighbors(Rules sRule, List<Cell> sCells) {
        int rTotal = 0;

        for (int i = -1; i <= 1 ; i++) { // Rows
            int tRow = _Row + i;
            for (int j = -1; j <= 1 ; j++) { // Cols
                int tCol = _Col + j;
                if (tRow >= 0 && tRow < sRule.getRows() &&
                    tCol >= 0 && tCol < sRule.getCols())
                {
                    if (tCol == _Col && tRow == _Row) {
                        // Exclude Same Cell
                    } else {
                        for (Cell k : sCells) {
                            Cell tCell = k.findCell(tCol, tRow);
                            if (tCell != null && tCell.getOldStat() == Stat.Alive) {
                                rTotal++;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return rTotal;
    }
}
