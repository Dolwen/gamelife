package com.ankamatest.gamelife;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class Client extends AsyncTask<Void, Void, Void> {

    private String _Address;
    private int _Port;
    private String _Response = "";
    private Context _Context;

    // Game parameters
    private Rules _Rule;

    // List of active Cells
    private List<Cell> _Cells;

    Client(String sAddress, int sPort, List<Cell> sCell, Rules sRule, Context sContext) {
        _Address = sAddress;
        _Port = sPort;
        _Cells = sCell;
        _Rule = sRule;
        _Context = sContext;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected Void doInBackground(Void... arg0) {

        Socket socket = null;
        try {
            socket = new Socket(_Address, _Port);

            InputStream inputStream = socket.getInputStream();
            ObjectInputStream objInputStream =  new ObjectInputStream(inputStream);

            boolean tNext = true;
            boolean tFirstEntry = true;

            // Load each saved Cell and add each to the List
            while(tNext) {
                if(inputStream.available() != 0) {
                    Object tObj = objInputStream.readObject();
                    if(tObj != null) {
                        if (tFirstEntry) {
                            // Get Rules at First
                            _Rule = (Rules)tObj;
                            tFirstEntry = false;
                        } else {
                            // Then each Cell
                            _Cells.add((Cell)tObj);
                        }
                    } else {
                        tNext = false;
                    }
                } else {
                    tNext = false;
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            _Response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            e.printStackTrace();
            _Response = "IOException: " + e.toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            _Response = "ClassException: " + e.toString();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {}

    @Override
    protected void onPostExecute(Void result) {
        Toast.makeText(_Context,_Response, Toast.LENGTH_LONG).show();
        super.onPostExecute(result);
    }
}